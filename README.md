# Worldpay exercise #

### How do I get set up? ###

This is a gradle/spring boot project. Tests run with the usual gradle command line: gradle test
The application runs as a Spring boot project, with an embedded H2 database.

Once the application is running, you can test it with a curl command. Please find below an example:

```
#!
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"description":"xyz", "price":"10", "currencyCode":"GBP"}' http://localhost:8080/offers/

```

There was no required feature in the exercise to retrieve offers. However, it is possible to access the H2 database at the following URL:


```
#!
http://localhost:8080/admin/dbconsole/

```

The following configuration needs to be used:
Driver Class:org.h2.Driver
JDBC URL:jdbc:h2:mem:testdb;MVCC=TRUE
User Name:sa

### Methodology ###

I wrote this application using TDD, with an inside/out approach. I tend to prefer this approach to have working components at each step of the development. It is then easier to achieve working iterations.

I commited each step of the development separately, for a reviewer to understand the thinking process. However, this kind of test cannot show a developer's TDD skills, since it is not possible to see the 3 steps of TDD in the commits: red, green, refactor. 

### Who do I talk to? ###

If you have any questions, please feel free to contact me:
cara.loic.pro@gmail.com