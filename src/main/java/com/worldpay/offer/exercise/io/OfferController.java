package com.worldpay.offer.exercise.io;

import com.worldpay.offer.exercise.app.OfferService;
import com.worldpay.offer.exercise.domain.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.ValidationException;

@RestController
public class OfferController {

    @Autowired
    OfferService offerService;

    @PostMapping("/offers/")
    public Offer createOffer(@RequestBody Offer offer){
        return offerService.createOffer(offer);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = ValidationException.class)
    public String handleValidationException(ValidationException e){
        return e.getMessage();
    }
}
