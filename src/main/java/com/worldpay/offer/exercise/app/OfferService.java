package com.worldpay.offer.exercise.app;

import com.worldpay.offer.exercise.domain.Offer;

public interface OfferService {

    Offer createOffer(Offer offer);
}
