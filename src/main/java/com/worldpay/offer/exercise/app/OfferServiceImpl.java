package com.worldpay.offer.exercise.app;

import com.worldpay.offer.exercise.app.repository.OfferRepository;
import com.worldpay.offer.exercise.app.validation.OfferValidationService;
import com.worldpay.offer.exercise.domain.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    OfferValidationService offerValidationService;

    @Autowired
    OfferRepository offerRepository;

    @Override
    public Offer createOffer(Offer offer) {
        if (offer == null) {
            throw new IllegalArgumentException("offer cannot be null");
        }

        offerValidationService.validateOffer(offer);
        return offerRepository.save(offer);
    }
}
