package com.worldpay.offer.exercise.app.repository;

import com.worldpay.offer.exercise.domain.Offer;
import org.springframework.data.repository.CrudRepository;

public interface OfferRepository extends CrudRepository<Offer, Long> {
}
