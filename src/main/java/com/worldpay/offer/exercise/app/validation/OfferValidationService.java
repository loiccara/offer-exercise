package com.worldpay.offer.exercise.app.validation;

import com.worldpay.offer.exercise.domain.Offer;
import org.springframework.stereotype.Service;

import javax.validation.*;
import java.util.Set;

@Service
public class OfferValidationService {

    private Validator validator;

    public OfferValidationService() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void validateOffer(Offer offer){
        Set<ConstraintViolation<Offer>> violations = validator.validate(offer);

        if (!violations.isEmpty()) {
            String message = "";
            for (ConstraintViolation<Offer> violation : violations) {
                message += violation.getPropertyPath() + ":" + violation.getMessage() + "\n";
            }
            throw new ValidationException(message);
        }
    }
}
