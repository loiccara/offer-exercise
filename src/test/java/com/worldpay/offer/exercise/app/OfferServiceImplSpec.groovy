package com.worldpay.offer.exercise.app

import com.worldpay.offer.exercise.app.repository.OfferRepository
import com.worldpay.offer.exercise.app.validation.OfferValidationService
import com.worldpay.offer.exercise.domain.Offer
import spock.lang.Specification

import javax.validation.ValidationException

class OfferServiceImplSpec extends Specification {

    private OfferService offerService
    private OfferRepository offerRepositoryMock

    def setup() {
        def offerValidationService = new OfferValidationService()
        offerRepositoryMock = Mock()
        offerService = new OfferServiceImpl(
                offerValidationService: offerValidationService,
                offerRepository: offerRepositoryMock
        )
    }

    def "should reject a null offer"() {
        when:
        offerService.createOffer(null)

        then:
        thrown(IllegalArgumentException)
    }

    def "should not throw any exception for a complete Offer and call the repository"(){
        given:
        def description = "delicious product made in France"
        def offer = new Offer(description, 10, "GBP")

        when:
        offerService.createOffer(offer)

        then:
        noExceptionThrown()
        1 * offerRepositoryMock.save(offer)
    }

    def "should throw an exception for an incomplete offer"(){
        given:
        def offer = new Offer("this offer will never pass the validation!!!", null, null)

        when:
        offerService.createOffer(offer)

        then:
        thrown ValidationException
        0 * offerRepositoryMock.save(offer)
    }
}
