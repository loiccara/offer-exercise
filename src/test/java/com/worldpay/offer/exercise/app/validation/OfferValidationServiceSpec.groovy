package com.worldpay.offer.exercise.app.validation

import com.worldpay.offer.exercise.domain.Offer
import spock.lang.Specification

import javax.validation.ValidationException

class OfferValidationServiceSpec extends Specification {
    private OfferValidationService offerValidationService

    def setup() {
        offerValidationService = new OfferValidationService()
    }

    def "should reject an incomplete offer "() {
        given:
        def offer = new Offer(description, price, currencyCode)

        when:
        offerValidationService.validateOffer(offer)

        then:
        def foundException = thrown(ValidationException)
        foundException.message == errorMessage

        where:
        description                    | price | currencyCode || errorMessage
        null                           | 10    | "GBP"        || "description:may not be null\n"
        "a wonderful and sexy produt!" | null  | "GBP"        || "price:may not be null\n"
        "a wonderful and sexy produt!" | 10    | null         || "currencyCode:may not be null\n"
    }

    def "should create an exception message with multiple lines for multiple errors"() {
        given:
        def offer = new Offer(null, null, null)

        when:
        offerValidationService.validateOffer(offer)

        then:
        def foundException = thrown(ValidationException)
        foundException.message.contains("description:may not be null\n")
        foundException.message.contains("price:may not be null\n")
        foundException.message.contains("currencyCode:may not be null\n")
    }
}
