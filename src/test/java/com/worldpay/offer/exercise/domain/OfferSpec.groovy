package com.worldpay.offer.exercise.domain

import spock.lang.Specification

import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

class OfferSpec extends Specification {

    private Validator validator

    def setup(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    def "should reject an Offer without a description"(){
        given:
        def offer = new Offer(null, 10, "GBP")

        when:
        def constraintViolations = validator.validate(offer)

        then:
        constraintViolations.size() == 1
        constraintViolations.first().propertyPath.toString() == "description"
        constraintViolations.first().message == "may not be null"
    }

    def "should reject an offer without a price"() {
        def offer = new Offer("super sexy description", null, "GBP")

        when:
        def constraintViolations = validator.validate(offer)

        then:
        constraintViolations.size() == 1
        constraintViolations.first().propertyPath.toString() == "price"
        constraintViolations.first().message == "may not be null"
    }

    def "should reject an offer without a currency"(){
        def offer = new Offer("super sexy description", 10, null)

        when:
        def constraintViolations = validator.validate(offer)

        then:
        constraintViolations.size() == 1
        constraintViolations.first().propertyPath.toString() == "currencyCode"
        constraintViolations.first().message == "may not be null"
    }
}
