package com.worldpay.offer.exercise.io


import com.worldpay.offer.exercise.app.OfferService
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import spock.lang.Specification

import javax.validation.ValidationException

class OfferControllerSpec extends Specification {

    private OfferController offerController
    private OfferService offerServiceMock
    private MockMvc mockMvc

    def setup(){
        offerServiceMock = Mock()
        offerController = new OfferController(offerService:offerServiceMock)
        mockMvc = MockMvcBuilders.standaloneSetup(offerController).build()
    }

    def "should reject return an HTTP error 400 for a ValidationException"(){
        when:
        def response =  mockMvc.perform(post("/offers/")
                .contentType(APPLICATION_JSON)
                .content('{"description":"blah"}')
        )

        then:
        1 * offerServiceMock.createOffer(_) >> {throw new ValidationException("price:may not be null")}
        response.andExpect(status().is4xxClientError())
                .andExpect(content().string("price:may not be null"))
    }
}
